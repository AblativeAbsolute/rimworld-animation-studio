﻿public enum ActorManipulationMode
{
    Pan,
    Rotate,
    Face,
}

public enum ActorGender
{
    Female,
    None,
    Male,
}

public enum CardinalDirection
{
    North,
    East,
    South,
    West,
}
