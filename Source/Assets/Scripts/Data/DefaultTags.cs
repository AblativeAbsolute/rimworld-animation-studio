﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RimWorldAnimationStudio
{
    public static class DefaultTags
    {
        public static List<string> defNames = new List<string>() { "Human", "Wolf_Timber", "Horse" };
        public static List<string> bodyParts = new List<string>() { "Penis", "Vagina", "Anus", "Breasts", "Mouth" };
        public static List<string> bodyDefTypes = new List<string>() { "Human", "Bird", "BeetleLike", "BeetleLikeWithClaw", "MechanicalCentipede", "MechanicalTermite", "Lancer", "Pikeman", "Monkey", "QuadrupedAnimalWithClawsTailAndJowl", "QuadrupedAnimalWithHooves", "QuadrupedAnimalWithHoovesAndHorn", "QuadrupedAnimalWithHoovesAndHump", "QuadrupedAnimalWithHoovesAndTusks", "QuadrupedAnimalWithHoovesTusksAndTrunk", "QuadrupedAnimalWithPaws", "QuadrupedAnimalWithPawsAndTail", "Scyther", "Snake", "TurtleLike" };
        public static List<string> sexTypes = new List<string>() { "Vaginal", "Anal", "Oral", "Masturbation", "DoublePenetration", "Boobjob", "Handjob", "Footjob", "Fingering", "Scissoring", "MutualMasturbation", "Fisting", "MechImplant", "Rimming", "Fellatio", "Cunnilingus", "Sixtynine" };
        public static List<string> interactionDefTypes = new List<string>() { "Bestiality_Anal", "Bestiality_Double_Penetration_M", "Bestiality_Oral", "Bestiality_Vaginal", "Breeding_Lick", "Breeding_Fingering", "Breeding_OralS", "Bestiality_Reverse_Anal", "Bestiality_Reverse_Double_Penetration_M", "Bestiality_Reverse_Handjob", "Bestiality_Reverse_Oral", "Bestiality_Reverse_Oral_Service", "Bestiality_Reverse_Vaginal", "AnimalSexChat", "Masturbation_AutoBreastjob", "Masturbation_AutoFellatio", "Masturbation_Breastjob", "Masturbation_HandjobA", "Masturbation_HandjobP", "Masturbation_HandjobV", "rjw_interaction_template", "Necro_Anal", "Necro_DoublePenetration", "Necro_DoublePenetrationM", "Necro_Vaginal", "Necro_Reverse_Anal", "Necro_Reverse_DoublePenetration", "Necro_Reverse_DoublePenetrationM", "Necro_Reverse_Vaginal", "Rape_MechImplant", "Rape_Anal", "Rape_Beakjob", "Rape_Breastjob", "Rape_Cunnilingus", "Rape_DoublePenetration", "Rape_DoublePenetrationM", "Rape_Fellatio", "Rape_Fingering", "Rape_Fisting", "Rape_Footjob", "Rape_Handjob", "Rape_Oral", "Rape_Rimming", "Rape_Scissoring", "Rape_Vaginal", "Rape_Reverse_Anal", "Rape_Reverse_Beakjob", "Rape_Reverse_Breastjob", "Rape_Reverse_Cunnilingus", "Rape_Reverse_DoublePenetration", "Rape_Reverse_DoublePenetrationM", "Rape_Reverse_Fellatio", "Rape_Reverse_Fingering", "Rape_Reverse_Fisting", "Rape_Reverse_Footjob", "Rape_Reverse_Handjob", "Rape_Reverse_Rimming", "Rape_Reverse_Scissoring", "Rape_Reverse_Vaginal", "Sex_Reverse_Anal", "Sex_Reverse_Beakjob", "Sex_Reverse_Breastjob", "Sex_Reverse_Cunnilingus", "Sex_Reverse_DoublePenetration", "Sex_Reverse_DoublePenetrationM", "Sex_Reverse_Fellatio", "Sex_Reverse_Fingering", "Sex_Reverse_Fisting", "Sex_Reverse_Footjob", "Sex_Reverse_Handjob", "Sex_Reverse_Rimming", "Sex_Reverse_Vaginal", "Sex_Anal", "Sex_Beakjob", "Sex_Breastjob", "Sex_Cunnilingus", "Sex_DoublePenetration", "Sex_DoublePenetrationM", "Sex_Fellatio", "Sex_Fingering", "Sex_Fisting", "Sex_Footjob", "Sex_Handjob", "Sex_MutualMasturbation", "Sex_Rimming", "Sex_Scissoring", "Sex_Sixtynine", "Sex_Vaginal" };
        public static List<string> soundDefs = new List<string>() { "None", "Sex", "Fuck", "Slimy", "Suck", "Cum" };
        public static List<string> actorLayers = new List<string>() { "LayingPawn", "PawnRope", "Projectile", "Pawn", "PawnUnused" };
        public static List<string> bodyTypes = new List<string>() { "Male", "Female", "Fat", "Hulk", "Thin" };
    }

    public static class CustomTags
    {
        public static List<string> defNames = new List<string>();
        public static List<string> bodyParts = new List<string>();
        public static List<string> bodyDefTypes = new List<string>();
        public static List<string> sexTypes = new List<string>();
        public static List<string> interactionDefTypes = new List<string>();
        public static List<string> soundDefs = new List<string>();
        public static List<string> bodyTypes = new List<string>();
    }

    [XmlRoot("CustomTagsHelper", IsNullable = false)]
    public class CustomTagsHelper
    {
        [XmlArray("defNames"), XmlArrayItem("li")] public List<string> defNames = new List<string>();
        [XmlArray("bodyParts"), XmlArrayItem("li")] public List<string> bodyParts = new List<string>();
        [XmlArray("bodyDefTypes"), XmlArrayItem("li")] public List<string> bodyDefTypes = new List<string>();
        [XmlArray("sexTypes"), XmlArrayItem("li")] public List<string> sexTypes = new List<string>();
        [XmlArray("interactionDefTypes"), XmlArrayItem("li")] public List<string> interactionDefTypes = new List<string>();
        [XmlArray("soundDefs"), XmlArrayItem("li")] public List<string> soundDefs = new List<string>();
        [XmlArray("bodyTypes"), XmlArrayItem("li")] public static List<string> bodyTypes = new List<string>();
    }
}
