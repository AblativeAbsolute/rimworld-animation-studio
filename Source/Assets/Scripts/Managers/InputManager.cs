﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class InputManager : Singleton<InputManager>
    {
        public Transform dialogBoxes;
        public GameObject keyframeSelector;

        private float lastUpdate = -1f;
        private float largeStep = 0.1f;
        private float smallStep = 0.03f;

        private bool isDragging;
        private Vector2 v0;
        
        public bool CanRepeatThisUpdate()
        {
            if (Time.unscaledTime > lastUpdate + Constants.actionRepeatSpeed)
            {
                lastUpdate = Time.unscaledTime;
                return true;
            }

            return false;
        }

        public bool IsModifierKeyHeld()
        {
            if (Input.GetKey(KeyCode.LeftShift)) return true;
            if (Input.GetKey(KeyCode.LeftControl)) return true;
            if (Input.GetKey(KeyCode.LeftAlt)) return true;
            if (Input.GetKey(KeyCode.LeftCommand)) return true;
            if (Input.GetKey(KeyCode.RightShift)) return true;
            if (Input.GetKey(KeyCode.RightControl)) return true;
            if (Input.GetKey(KeyCode.RightAlt)) return true;
            if (Input.GetKey(KeyCode.RightCommand)) return true;

            return false;
        }

        public bool IsDialogBoxIsActive()
        {
            foreach (Transform child in dialogBoxes)
            { if (child.gameObject.activeSelf) return true; }

            return false;
        }

        public void Update()
        {
            if (IsDialogBoxIsActive()) return;
            if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<InputField>()?.isFocused == true) return;
            bool canRepeatThisUpdate = CanRepeatThisUpdate();

            // Check keybinds
            foreach (Keybind keybind in KeybindConfig.GetAllKeybinds())
            {
                if (IsModifierKeyHeld() && keybind.keyModifiers.NullOrEmpty()) goto nextKeybind;

                foreach (KeyCode modKeyCode in keybind.keyModifiers)
                { if (Input.GetKey(modKeyCode) == false) goto nextKeybind; }

                if (keybind.repeatable && canRepeatThisUpdate)
                { if (Input.GetKey(keybind.keyCode) == false) goto nextKeybind; }

                else
                { if (Input.GetKeyDown(keybind.keyCode) == false) goto nextKeybind; }

                //Debug.Log(keybind.command);
                Invoke(keybind.command, 0f); return;

                nextKeybind:;
            }

            // Drag selection
            if (Input.GetMouseButtonDown(0))
            { StartKeyframeSelectionDrag(); }

            else if (Input.GetMouseButtonUp(0))
            { EndKeyframeSelectionDrag(); }

            else if (isDragging)
            { UpdateKeyframeSelector(); }

            else
            { HideKeyframeSelector(); }
        }

        public void StartKeyframeSelectionDrag()
        {
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer.position = Input.mousePosition;

            List<RaycastResult> raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);

            if (raycastResults.Count > 0)
            {
                foreach (var go in raycastResults)
                {
                    //Debug.Log(go.gameObject);
                    if (go.gameObject?.GetComponent<Button>() != null) return;
                    if (go.gameObject?.GetComponent<IDragHandler>() != null) return;
                    if (go.gameObject?.GetComponentInParent<IDragHandler>() != null) return;
                }
            }

            v0 = (Vector2)Input.mousePosition;
            isDragging = true;
        }

        public void EndKeyframeSelectionDrag()
        {
            if (isDragging == false) return;
            isDragging = false;

            Vector2 v1 = (Vector2)Input.mousePosition;
            List<int> keyframeIDs = new List<int>();
            IEnumerable<KeyframeSlider> sliders = Selectable.allSelectablesArray.Select(x => x.GetComponent<KeyframeSlider>());

            foreach (KeyframeSlider slider in sliders)
            {
                Transform handle = slider?.transform?.FindDeepChild("Handle");
                if (handle == null) continue;

                Vector2 vi = (Vector2)handle.GetComponent<RectTransform>().position;

                if (vi.x < Mathf.Min(v0.x, v1.x)) continue;
                if (vi.x > Mathf.Max(v0.x, v1.x)) continue;
                if (vi.y < Mathf.Min(v0.y, v1.y)) continue;
                if (vi.y > Mathf.Max(v0.y, v1.y)) continue;

                keyframeIDs.Add(slider.keyframeID);
            }

            if (keyframeIDs.NotNullOrEmpty())
            { Workspace.keyframeID = keyframeIDs; }
        }

        public void UpdateKeyframeSelector()
        {
            Vector2 v1 = (Vector2)Input.mousePosition;
            Vector2 pos = new Vector2(Mathf.Min(v0.x, v1.x), Mathf.Min(v0.y, v1.y));
            Vector2 dim = new Vector2(Mathf.Abs(v1.x - v0.x), Mathf.Abs(v1.y - v0.y));

            keyframeSelector.GetComponent<RectTransform>().anchoredPosition = pos;
            keyframeSelector.GetComponent<RectTransform>().sizeDelta = dim;

            if (keyframeSelector.activeSelf == false)
            { keyframeSelector.SetActive(true); }
        }

        public void HideKeyframeSelector()
        {
            if (keyframeSelector.activeSelf)
            { keyframeSelector.SetActive(false); }
        }

        public void QuitApplication()
        {
            ApplicationManager.Instance.TryToCloseApplication();
        }

        public void NewAnimation()
        {
            ApplicationManager.Instance.TryToMakeNewAnimation();
        }

        public void SaveAnimation()
        {
            ApplicationManager.Instance.TryToSaveAnimation();
        }

        public void SaveAnimationAs()
        {
            ApplicationManager.Instance.TryToSaveAnimationAs();
        }

        public void LoadAnimation()
        {
            ApplicationManager.Instance.TryToLoadAnimation();
        }

        public void UndoAction()
        {
            if (Workspace.animationDef == null) return;
            Workspace.Undo();
        }

        public void RedoAction()
        {
            if (Workspace.animationDef == null) return;
            Workspace.Redo();
        }

        public void ToggleAnimationPreview()
        {
            if (Workspace.animationDef == null) return;
            Workspace.IsAnimating = !Workspace.IsAnimating;
        }

        public void AddKeyframe()
        {
            if (Workspace.animationDef == null) return;
            Workspace.GetCurrentPawnAnimationClip().AddPawnKeyframe();
        }

        public void CopyKeyframes()
        {
            if (Workspace.animationDef == null) return;
            Workspace.GetCurrentPawnAnimationClip().CopyPawnKeyframes();
        }

        public void PasteKeyframes()
        {
            if (Workspace.animationDef == null) return;
            Workspace.GetCurrentPawnAnimationClip().PastePawnKeyframes();
        }

        public void DeleteKeyframes()
        {
            if (Workspace.animationDef == null) return;

            foreach (int keyframeID in Workspace.keyframeID)
            { Workspace.GetAnimationClipThatOwnsKeyframe(keyframeID).RemovePawnKeyframe(keyframeID); }
        }

        public void ActorMovementMode()
        {
            Workspace.actorManipulationMode = (ActorManipulationMode)0;
        }

        public void ActorRotateMode()
        {
            Workspace.actorManipulationMode = (ActorManipulationMode)1;
        }

        public void ActorFacingMode()
        {
            Workspace.actorManipulationMode = (ActorManipulationMode)2;
        }

        public void AdjustActorUpward()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.up * largeStep);
        }

        public void AdjustActorDownward()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.down * largeStep);
        }

        public void AdjustActorLeftward()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.left * largeStep);
        }

        public void AdjustActorRightward()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.right * largeStep);
        }

        public void AdjustActorUpwardSmall()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.up * smallStep);
        }

        public void AdjustActorDownwardSmall()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.down * smallStep);
        }

        public void AdjustActorLeftwardSmall()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.left * smallStep);
        }

        public void AdjustActorRightwardSmall()
        {
            if (Workspace.animationDef == null) return;

            PawnKeyframe keyframe = Workspace.GetCurrentPawnKeyframe(true);
            keyframe.AdjustActor(Vector2.right * smallStep);
        }

        public void CycleActorBodyPartSelecion()
        {
            if (Workspace.animationDef == null) return;

            ActorBody actorBody = AnimationController.Instance.actorBodies.GetComponentsInChildren<ActorBody>().FirstOrDefault(x => x.actorID == Workspace.ActorID);
            List<ActorBodyPart> actorBodyParts = actorBody?.GetComponentsInChildren<ActorBodyPart>()?.Where(x => x.gameObject.activeInHierarchy)?.ToList();

            if (actorBodyParts.NullOrEmpty()) return;

            if (Workspace.selectedBodyPart == null)
            { actorBodyParts[0].Activate(); return; }

            else
            {
                for (int i = 0; i < actorBodyParts.Count; i++)
                {
                    ActorBodyPart part = actorBodyParts[i];

                    if (part == Workspace.selectedBodyPart)
                    {
                        if (i < actorBodyParts.Count - 1)
                        { actorBodyParts[i + 1].Activate(); return; }

                        else
                        { actorBody.Activate(); return; }
                    }
                }
            }
        }

        public void ToPreviousActor()
        {
            if (Workspace.animationDef == null) return;

            Workspace.selectedBodyPart = null;
            Workspace.ActorID = Mathf.Clamp(Workspace.ActorID - 1, 0, Workspace.animationDef.Actors.Count - 1);
        }

        public void ToNextActor()
        {
            if (Workspace.animationDef == null) return;

            Workspace.selectedBodyPart = null;
            Workspace.ActorID = Mathf.Clamp(Workspace.ActorID + 1, 0, Workspace.animationDef.Actors.Count - 1);
        }

        public void ToPreviousTick()
        {
            if (Workspace.animationDef == null) return;
            Workspace.StageTick = Mathf.Clamp(Workspace.StageTick - 1, Constants.minTick, Workspace.StageWindowSize);
        }

        public void ToNextTick()
        {
            if (Workspace.animationDef == null) return;
            Workspace.StageTick = Mathf.Clamp(Workspace.StageTick + 1, Constants.minTick, Workspace.StageWindowSize);
        }

        public void ToFirstTick()
        {
            if (Workspace.animationDef == null) return;
            Workspace.StageTick = Constants.minTick;
        }

        public void ToLastTick()
        {
            if (Workspace.animationDef == null) return;
            Workspace.StageTick = Workspace.StageWindowSize;
        }

        public void ToPreviousKey()
        {
            if (Workspace.animationDef == null) return;
            PawnKeyframe keyframe = Workspace.GetPreviousKeyframe(Workspace.ActorID);
            if (keyframe != null) Workspace.StageTick = keyframe.atTick.Value;
        }

        public void ToNextKey()
        {
            if (Workspace.animationDef == null) return;
            PawnKeyframe keyframe = Workspace.GetNextKeyframe(Workspace.ActorID);
            if (keyframe != null) Workspace.StageTick = keyframe.atTick.Value;
        }

        public void ToPreviousStage()
        {
            if (Workspace.animationDef == null) return;

            int prevStageID = Workspace.StageID;
            Workspace.StageID = Mathf.Clamp(Workspace.StageID - 1, 0, Workspace.animationDef.AnimationStages.Count - 1);

            if (Workspace.StageID != prevStageID)
            { Workspace.RecordEvent("Stage selected"); }
        }

        public void ToNextStage()
        {
            if (Workspace.animationDef == null) return;

            int prevStageID = Workspace.StageID;
            Workspace.StageID = Mathf.Clamp(Workspace.StageID + 1, 0, Workspace.animationDef.AnimationStages.Count - 1);

            if (Workspace.StageID != prevStageID)
            { Workspace.RecordEvent("Stage selected"); }
        }

        public void CenterView()
        {
            Camera.main.GetComponent<CameraController>().ResetCamera();
        }

        public void StretchKeyframesToggle()
        {
            Workspace.stretchKeyframes = !Workspace.stretchKeyframes;
        }

        public void OpenProjectHome()
        {
            if (Uri.IsWellFormedUriString(Constants.projectHome, UriKind.RelativeOrAbsolute))
            { Application.OpenURL(Constants.projectHome); }
        }

        public void OpenProjectWiki()
        {
            if (Uri.IsWellFormedUriString(Constants.projectWiki, UriKind.RelativeOrAbsolute))
            { Application.OpenURL(Constants.projectWiki); }
        }

        public void AddActor() 
        {
            if (Workspace.animationDef == null) return;

            Workspace.animationDef.AddActor();
        }

        public void RemoveActor() 
        {
            if (Workspace.animationDef == null) return;

            Workspace.animationDef.RemoveActor();
        }    

        public void AddStage()
        {
            if (Workspace.animationDef == null) return;

            Workspace.animationDef.AddAnimationStage();
        }

        public void CloneStage()
        {
            if (Workspace.animationDef == null) return;

            Workspace.animationDef.CloneAnimationStage();
        }

        public void RemoveStage()
        {
            if (Workspace.animationDef == null) return;

            Workspace.animationDef.RemoveAnimationStage();
        }
    }
}
