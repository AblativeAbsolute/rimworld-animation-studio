﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class StageCardManager : Singleton<StageCardManager>
    {
        public StageCard stageCardPrefab;

        public void Start()
        {
            EventsManager.onAnimationChanged.AddListener(delegate { Initialize(); });
            EventsManager.onStageIDChanged.AddListener(delegate { Initialize(); });
            EventsManager.onStageCountChanged.AddListener(delegate { Initialize(); });

            Initialize();
        }

        public void Initialize()
        {
            int stageCount = Workspace.animationDef.AnimationStages.Count;
            int childCount = GetComponentsInChildren<StageCard>().Count();

            for (int i = 0; i < Mathf.Max(stageCount, childCount); i++)
            {
                // Add new stage cards as required
                if (i >= childCount)
                { Instantiate(stageCardPrefab, transform); }

                // Get objects to update
                StageCard stageCard = GetComponentsInChildren<StageCard>()[i];

                // Update values
                if (i < stageCount)
                { stageCard.Initialize(Workspace.animationDef.AnimationStages[i].StageName); }

                // Remove excess objects as required
                else
                { Destroy(stageCard.gameObject); }
            }
        }
    }
}
