﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace RimWorldAnimationStudio
{
    public static class EventsManager
    {
        // Event classes
        public class WorkspaceIntEvent : UnityEvent<int> { }
        public class ActorEvent : UnityEvent<Actor> { }
        public class AnimationStageEvent : UnityEvent<AnimationStage> { }
        public class PawnAnimationClipEvent : UnityEvent<PawnAnimationClip> { }
        public class PawnKeyframeEvent : UnityEvent<PawnKeyframe> { }
        public class ActorAddonEvent : UnityEvent<ActorAddon> { }
        public class AddonKeyframeEvent : UnityEvent<AddonKeyframe> { }
        public class ActorBodyEvent : UnityEvent<ActorBody> { }
        public class ActorBodyPartEvent : UnityEvent<ActorBodyPart> { }

        // Event list
        public static UnityEvent onAnimationTimelinesChanged = new UnityEvent();
        public static UnityEvent onAnimationToggled = new UnityEvent();
        public static UnityEvent onAnimationChanged = new UnityEvent();
        public static UnityEvent onAnimationDefChanged = new UnityEvent();
        public static WorkspaceIntEvent onActorIDChanged = new WorkspaceIntEvent();
        public static WorkspaceIntEvent onStageIDChanged = new WorkspaceIntEvent();
        public static WorkspaceIntEvent onStageTickChanged = new WorkspaceIntEvent();
        public static WorkspaceIntEvent onStageCountChanged = new WorkspaceIntEvent();
        public static WorkspaceIntEvent onActorCountChanged = new WorkspaceIntEvent();
        public static WorkspaceIntEvent onKeyframeCountChanged = new WorkspaceIntEvent();
        public static ActorEvent onActorChanged = new ActorEvent();
        public static AnimationStageEvent onAnimationStageChanged = new AnimationStageEvent();
        public static AnimationStageEvent onStageWindowSizeChanged = new AnimationStageEvent();
        public static PawnAnimationClipEvent onPawnAnimationClipChanged = new PawnAnimationClipEvent();
        public static PawnKeyframeEvent onPawnKeyframeChanged = new PawnKeyframeEvent();
        public static ActorAddonEvent onActorAddonChanged = new ActorAddonEvent();
        public static AddonKeyframeEvent onAddonKeyframeChanged = new AddonKeyframeEvent();
        public static UnityEvent onDefNamesChanged = new UnityEvent();
        public static ActorBodyEvent onActorBodySelected = new ActorBodyEvent();
        public static ActorBodyPartEvent onActorBodyPartSelected = new ActorBodyPartEvent();

        // Event invoking
        public static void OnAnimationTimelinesChanged() { onAnimationTimelinesChanged.Invoke(); }
        public static void OnAnimationToggled() { onAnimationToggled.Invoke(); }
        public static void OnAnimationChanged() { onAnimationChanged.Invoke(); }
        public static void OnAnimationDefChanged() { onAnimationDefChanged.Invoke(); }
        public static void OnActorIDChanged() { onActorIDChanged.Invoke(Workspace.ActorID); }
        public static void OnStageIDChanged() { onStageIDChanged.Invoke(Workspace.ActorID); }
        public static void OnStageTickChanged() { onStageTickChanged.Invoke(Workspace.StageTick); }
        public static void OnStageCountChanged() { onStageCountChanged.Invoke(Workspace.animationDef.AnimationStages.Count); }
        public static void OnActorCountChanged() { onActorCountChanged.Invoke(Workspace.animationDef.Actors.Count); }
        public static void OnKeyframeCountChanged(PawnAnimationClip clip) { onKeyframeCountChanged.Invoke(clip.Keyframes.Count); }
        public static void OnActorChanged(Actor actor) { onActorChanged.Invoke(actor); }
        public static void OnAnimationStageChanged(AnimationStage stage) { onAnimationStageChanged.Invoke(stage); }
        public static void OnStageWindowSizeChanged(AnimationStage stage) { onStageWindowSizeChanged.Invoke(stage); }
        public static void OnPawnAnimationClipChanged(PawnAnimationClip clip) { onPawnAnimationClipChanged.Invoke(clip); }
        public static void OnPawnKeyframeChanged(PawnKeyframe keyframe) { onPawnKeyframeChanged.Invoke(keyframe); }
        public static void OnActorAddonChanged(ActorAddon actorAddon) { onActorAddonChanged.Invoke(actorAddon); }
        public static void OnAddonKeyframeChanged(AddonKeyframe addonKeyframe) { onAddonKeyframeChanged.Invoke(addonKeyframe); }
        public static void OnDefNamesChanged() { onDefNamesChanged.Invoke(); }
        public static void OnActorBodySelected(ActorBody actorBody) { onActorBodySelected.Invoke(actorBody); }
        public static void OnActorBodyPartSelected(ActorBodyPart bodyPart) { onActorBodyPartSelected.Invoke(bodyPart); }
    }
}
