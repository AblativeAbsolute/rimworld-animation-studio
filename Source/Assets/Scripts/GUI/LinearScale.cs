﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class LinearScale : Singleton<LinearScale>
    {
        public Transform animationTimelines;
        public int targetDivisions = 30;
        public List<int> divisionBands = new List<int>() { 5, 10, 25, 50, 100, 250, 500, 1000 };
        public GameObject linearScaleTickPrefab;

        private List<int> divisions = new List<int>();
        private float minDiff = -1f;
 
        public void Start()
        {
            EventsManager.onAnimationChanged.AddListener(delegate { UpdateLinearScale(); });
            EventsManager.onStageIDChanged.AddListener(delegate { UpdateLinearScale(); });
            EventsManager.onStageWindowSizeChanged.AddListener(delegate { UpdateLinearScale(); });

            UpdateLinearScale();
        }

        public void UpdateLinearScale()
        {
            if (Workspace.animationDef == null) return;

            LayoutRebuilder.ForceRebuildLayoutImmediate(animationTimelines.GetComponent<RectTransform>());
            minDiff = -1f;

            foreach (int division in divisionBands)
            {
                float numDivisions = (float)Workspace.StageWindowSize / division;

                if (minDiff >= 0f && Mathf.Abs(targetDivisions - numDivisions) > minDiff) continue;
                minDiff = Mathf.Abs(targetDivisions - numDivisions);

                int i = 0;
                divisions.Clear();

                while (i + division <= Workspace.StageWindowSize)
                {
                    i += division;
                    divisions.Add(i);
                }
            }

            foreach (Transform child in transform)
            { Destroy(child.gameObject); }

            foreach (int division in divisions)
            {
                GameObject obj = Instantiate(linearScaleTickPrefab, transform);
                obj.GetComponentInChildren<Text>().text = division.ToString();

                float xOffset = ((float)(division - Constants.minTick) / (Workspace.StageWindowSize - Constants.minTick)) * transform.parent.GetComponent<RectTransform>().rect.width;
                obj.GetComponent<RectTransform>().localPosition = new Vector3(xOffset, 0, 0);
            }
        }    
    }
}
