﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class DropdownMenu : MonoBehaviour, IDeselectHandler
    {
        public Transform dropdownMenu;

        public void OpenMenu()
        {
            dropdownMenu.gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(this.gameObject); 
        }

        public void CloseMenu()
        {
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                Transform child = transform.FindDeepChild(EventSystem.current.currentSelectedGameObject.name);
                
                if (child != null)
                {
                    //EventSystem.current.SetSelectedGameObject(this.gameObject);
                    return;
                }
            }

            dropdownMenu.gameObject.SetActive(false);
        }

        public void OnDeselect(BaseEventData eventData)
        {
            Invoke("CloseMenu", 0.15f);
        }
    }
}
