﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class ButtonWithKeyCode : Button
    {
        public KeyCode keyCode;

        public void Update()
        {
            if (Input.GetKeyDown(keyCode))
            { onClick.Invoke(); }
        }
    }
}
