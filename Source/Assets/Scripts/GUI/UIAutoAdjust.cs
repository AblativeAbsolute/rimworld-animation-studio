﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public class UIAutoAdjust : MonoBehaviour
    {
        public RectTransform driverTransform;

        private RectTransform rectTransform;
        private RectTransform parentTransform;

        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            parentTransform = transform.parent.GetComponent<RectTransform>();
        }

        private void Update()
        {
            float targetHeight = parentTransform.rect.height - driverTransform.rect.height;
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, targetHeight);
        }
    }
}
