﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class InactiveDuringAnimationPreview : MonoBehaviour
    {
        private InputField inputfield;

        private void Start()
        {
            inputfield = GetComponent<InputField>();
        }

        private void Update()
        {
            inputfield.interactable = Workspace.IsAnimating == false;
        }
    }
}
