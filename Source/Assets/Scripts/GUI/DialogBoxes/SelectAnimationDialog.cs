﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class SelectAnimationDialog : DialogBox
    {
        public void Initialize(AnimationDefs defs)
        {
            Transform contentWindow = transform.FindDeepChild("Content");
            Reset();

            for (int i = 0; i < defs.animationDefs.Count; i++)
            {
                AnimationDef animationDef = defs.animationDefs[i];

                Transform _optionButton = AddCloneObjectToParent(contentWindow).transform;
                _optionButton.Find("Text").GetComponent<Text>().text = animationDef.DefName;

                Button buttonComp = _optionButton.GetComponent<Button>();
                buttonComp.onClick.AddListener(delegate { Pop(); ApplicationManager.Instance.LoadAnimation(animationDef); });
            }
        }

        public void Reset()
        {
            Transform contentWindow = transform.FindDeepChild("Content");
            RemoveCloneObjectsFromParent(contentWindow);
        }
    }
}
