﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class SelectActorAddonsDialog : DialogBox
    {
        private List<ActorAddonCard> actorAddonCards = new List<ActorAddonCard>();

        public void AddActorAddonCard(ActorAddonCard actorAddonCard)
        {
            actorAddonCards.Add(actorAddonCard);
        }
    }
}
