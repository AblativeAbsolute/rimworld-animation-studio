﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using SFB;
using System.IO;

namespace RimWorldAnimationStudio
{
    public class RaceSettingsDialog : DialogBox
    {
        public Dropdown raceSelectDropdown;
        public Transform raceSettingsWindow;
        public Toggle isHumanoidToggle;
        public InputField scaleField;

        protected override void OnEnable()
        {
            raceSelectDropdown.ClearOptions();
            raceSelectDropdown.AddOptions(DefaultTags.defNames.Concat(CustomTags.defNames).ToList());
            
            base.OnEnable();
        }

        public override void Initialize(bool addedNewTag = false)
        {
            Reset();
         
            PawnRaceDef pawnRaceDef = GetCurrentRaceDef();
            if (pawnRaceDef == null) return;

            isHumanoidToggle.SetIsOnWithoutNotify(pawnRaceDef.isHumanoid);

            Text bodyGraphicsTitle = AddCloneObjectToParent(raceSettingsWindow, 2).GetComponent<Text>();
            bodyGraphicsTitle.text = "Body graphic filepaths";

            List<string> allTags = pawnRaceDef.isHumanoid ? DefaultTags.bodyTypes : new List<string>() { "None" };

            foreach (string bodyType in allTags)
            {
                string _bodyType = bodyType;

                if (pawnRaceDef.isHumanoid)
                {
                    Text bodyTypeTitle = AddCloneObjectToParent(raceSettingsWindow, 2).GetComponent<Text>();
                    bodyTypeTitle.text = bodyType;
                }

                for (int i = 2; i >= 0; i--)
                {
                    CardinalDirection facing = (CardinalDirection)i;

                    GameObject filepath = AddCloneObjectToParent(raceSettingsWindow, 0);
                    filepath.GetComponent<Text>().text = facing.ToString();
                    filepath.transform.Find("FilepathButton").GetComponent<Button>().onClick.AddListener(delegate
                    {
                        SetBodyTypeGraphicPath(pawnRaceDef, facing, _bodyType);
                    });
                    filepath.transform.FindDeepChild("FilepathLabel").GetComponent<Text>().text = pawnRaceDef.GetBodyTypeGraphicPath(facing, _bodyType);
                }

                AddCloneObjectToParent(raceSettingsWindow, 3);
            }

            if (pawnRaceDef.isHumanoid)
            {
                Text headGraphics = AddCloneObjectToParent(raceSettingsWindow, 2).GetComponent<Text>();
                headGraphics.text = "Head graphic filepaths";

                for (int i = 2; i >= 0; i--)
                {
                    CardinalDirection facing = (CardinalDirection)i;

                    GameObject filepath = AddCloneObjectToParent(raceSettingsWindow, 0);
                    filepath.GetComponent<Text>().text = facing.ToString();
                    filepath.transform.Find("FilepathButton").GetComponent<Button>().onClick.AddListener(delegate
                    {
                        SetHeadGraphicPath(pawnRaceDef, facing);
                    });
                    filepath.transform.FindDeepChild("FilepathLabel").GetComponent<Text>().text = pawnRaceDef.GetHeadGraphicPath(facing);
                }

                AddCloneObjectToParent(raceSettingsWindow, 3);
            }

            scaleField.text = string.Format("{0:0.000}", pawnRaceDef.scale);
        }

        public void Reset()
        {
            RemoveCloneObjectsFromParent(raceSettingsWindow);         
        }

        public void SetIsHumanoid()
        {
            PawnRaceDef pawnRaceDef = GetCurrentRaceDef();
            if (pawnRaceDef == null) return;

            pawnRaceDef.isHumanoid = isHumanoidToggle.isOn;

            Initialize();
        }

        public void SetHeadGraphicPath(PawnRaceDef pawnRaceDef, CardinalDirection direction)
        {
            var paths = StandaloneFileBrowser.OpenFilePanel("Select texture File", "", "png", false);

            if (paths == null || paths.Any() == false || File.Exists(paths[0]) == false)
            { Debug.LogWarning("Selected file was null or invalid"); return; }

            pawnRaceDef.SetHeadGraphicPath(paths[0], direction);

            Initialize();
        }

        public void SetBodyTypeGraphicPath(PawnRaceDef pawnRaceDef, CardinalDirection direction, string bodyType)
        {
            var paths = StandaloneFileBrowser.OpenFilePanel("Select texture File", "", "png", false);

            if (paths == null || paths.Any() == false || File.Exists(paths[0]) == false)
            { Debug.LogWarning("Selected file was null or invalid"); return; }

            pawnRaceDef.SetBodyTypeGraphicPath(paths[0], direction, bodyType);

            Initialize();
        }

        public PawnRaceDef GetCurrentRaceDef()
        {
            string pawnRaceDefName = raceSelectDropdown.value < raceSelectDropdown.options.Count ? raceSelectDropdown.options[raceSelectDropdown.value].text : "Human";
            if (pawnRaceDefName == null || pawnRaceDefName == "") pawnRaceDefName = "Human";

            return PawnRaceDefs.GetNamed(pawnRaceDefName);
        }

        public void SetRaceScale()
        {
            PawnRaceDef pawnRaceDef = GetCurrentRaceDef();
            if (pawnRaceDef == null) return;

            float scale = float.Parse(scaleField.text);
            pawnRaceDef.scale = Mathf.Clamp(scale, 0.01f, 100f);

            scaleField.text = string.Format("{0:0.000}", pawnRaceDef.scale);
        }
    }
}
