﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class SelectSoundDefDialog : DialogBox
    {
        public override void Initialize(bool addedNewTag = false)
        {
            IEnumerable<string> allTags = DefaultTags.soundDefs.Concat(CustomTags.soundDefs);
            string placeHolderText = "Enter new sound def...";

            if (Workspace.animationDef == null) return;

            Transform contentWindow = transform.FindDeepChild("Content");
            Reset();

            for (int i = 0; i < allTags.Count(); i++)
            {
                string tag = allTags.ElementAt(i);

                Transform _optionToggle = AddCloneObjectToParent(contentWindow).transform;
                _optionToggle.Find("Text").GetComponent<Text>().text = tag;

                Toggle toggleComp = _optionToggle.GetComponent<Toggle>();
                toggleComp.isOn = Workspace.GetCurrentOrPreviousKeyframe(Workspace.ActorID)?.SoundEffect == tag;
                toggleComp.onValueChanged.AddListener(delegate 
                {
                    PawnKeyframe keyframe = Workspace.GetCurrentOrPreviousKeyframe(Workspace.ActorID);

                    if (keyframe != null)
                    { keyframe.SoundEffect = tag; }
                    
                    Workspace.RecordEvent("Keyframe sound effect");
                });

                if (CustomTags.soundDefs.Contains(tag))
                {
                    Button deleteButton = _optionToggle.Find("DeleteButton").GetComponent<Button>();
                    deleteButton.gameObject.SetActive(true);
                    deleteButton.onClick.AddListener(delegate { RemoveCustomTag(ref CustomTags.soundDefs, tag); });
                }

                if (addedNewTag && i == allTags.Count() - 1)
                { toggleComp.isOn = true; }

                toggleComp.group = contentWindow.GetComponent<ToggleGroup>();
            }

            Transform _optionField = AddCloneObjectToParent(contentWindow, 1).transform;
            _optionField.Find("Placeholder").GetComponent<Text>().text = placeHolderText;

            InputField fieldComp = _optionField.GetComponent<InputField>();
            fieldComp.onEndEdit.AddListener(delegate { AddCustomTag(fieldComp, ref DefaultTags.soundDefs, ref CustomTags.soundDefs); });
        }

        public void Reset()
        {
            Transform contentWindow = transform.FindDeepChild("Content");
            RemoveCloneObjectsFromParent(contentWindow);
        }
    }
}
