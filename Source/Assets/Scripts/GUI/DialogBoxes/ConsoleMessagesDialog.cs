﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class ConsoleMessagesDialog : MonoBehaviour
    {
        public bool isExpanded = false;
        public GameObject consoleWindow;
        public Transform logContent;
        public Text stackTrace;
        public Text currentMessage;
        public Text logMessagePrefab;
        public int maxMessages = 500;

        private int currentMessages = 0;

        public void Start()
        {
            Application.logMessageReceived += LogMessage;
        }

        public void ToggleExpand()
        {
            isExpanded = !isExpanded;

            consoleWindow.SetActive(isExpanded);
        }

        public void LogMessage(string logString, string stackTrace, LogType type)
        {
            if (currentMessages > maxMessages) return;
            currentMessages++; 

            currentMessage.text = logString;

            Text logMessage = Instantiate(logMessagePrefab, logContent);
            logMessage.text = logString;

            logMessage.GetComponent<Button>().onClick.AddListener(delegate { OpenStackTrace(stackTrace); });

            if (type == LogType.Warning)
            {
                currentMessage.color = Constants.ColorRichOrange;
                logMessage.color = Constants.ColorRichOrange;
            }

            else if (type == LogType.Exception || type == LogType.Error)
            {
                currentMessage.color = Constants.ColorRed;
                logMessage.color = Constants.ColorRed; 
            }

            else
            {
                currentMessage.color = Constants.ColorDarkGrey;
                logMessage.color = Constants.ColorDarkGrey;
            }
        }

        public void OpenStackTrace(string stackTrace)
        {
            this.stackTrace.text = stackTrace;
        }
    }
}
