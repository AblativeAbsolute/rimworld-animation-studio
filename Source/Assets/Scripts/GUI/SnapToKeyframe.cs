﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class SnapToKeyframe : MonoBehaviour, IDragHandler
    {
        private Slider slider;

        public void Start()
        {
            slider = GetComponent<Slider>();
        }

        public void OnDrag(PointerEventData eventData)
        {
            int targetTick = Workspace.FindClosestKeyFrameAtTick((int)slider.value, Mathf.CeilToInt(Workspace.StageWindowSize * 0.01f));

            if (Input.GetKey(KeyCode.LeftShift))
            { slider.value = (float)targetTick; }
        }
    }
}
