﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class AnimationControlCard : MonoBehaviour
    {
        public InputField currentTimeField;
        public InputField stageWindowLengthField;
        public InputField playBackSpeedField;
        public Button playToggleButton;
        public Slider stageTimelineSlider;

        private void Start()
        {
            EventsManager.onAnimationChanged.AddListener(delegate { UpdateGUI(); });
            EventsManager.onStageIDChanged.AddListener(delegate { UpdateGUI(); });
            EventsManager.onStageTickChanged.AddListener(delegate { UpdateGUI(); });
            EventsManager.onAnimationToggled.AddListener(delegate { playToggleButton.image.color = Workspace.IsAnimating ? Constants.ColorGoldYellow : Constants.ColorWhite; });

            stageTimelineSlider.onValueChanged.AddListener(delegate { OnStageTimelineSliderChange(); });
            currentTimeField.onEndEdit.AddListener(delegate { OnCurrentTimeFieldChange(); });
            stageWindowLengthField.onEndEdit.AddListener(delegate { OnStageWindowLengthFieldChange(); });
            playBackSpeedField.onEndEdit.AddListener(delegate { OnPlayBackSpeedChange(); });

            UpdateGUI();
        }

        public void OnStageTimelineSliderChange()
        {
            Workspace.StageTick = (int)stageTimelineSlider.value;           
        }

        public void OnPlayBackSpeedChange()
        {
            Workspace.PlayBackSpeed = float.Parse(playBackSpeedField.text);
        }

        public void OnCurrentTimeFieldChange()
        {
            Workspace.StageTick = Mathf.Clamp(int.Parse(currentTimeField.text), Constants.minTick, Workspace.StageWindowSize);

            UpdateGUI();
        }

        public void OnStageWindowLengthFieldChange()
        {
            int.TryParse(stageWindowLengthField.text, out int newStageWindowSize);
            newStageWindowSize = Mathf.Clamp(newStageWindowSize, Constants.minAnimationClipLength, Constants.maxAnimationClipLength);

            Debug.Log("Resizing animation clip length to " + newStageWindowSize.ToString() + " ticks.");

            if (Workspace.stretchKeyframes)
            { Workspace.GetCurrentAnimationStage().StretchStageWindow(newStageWindowSize); }

            else
            {
                Workspace.GetCurrentAnimationStage().ResizeStageWindow(newStageWindowSize);

                foreach (PawnAnimationClip clip in Workspace.GetCurrentAnimationStage().AnimationClips)
                {
                    List<PawnKeyframe> keyframes = clip.Keyframes.Where(x => x.atTick > newStageWindowSize)?.ToList();

                    if (keyframes.NullOrEmpty())
                    { continue; }

                    foreach (PawnKeyframe keyframe in keyframes)
                    {
                        if (clip.Keyframes.Count <= 2)
                        { break; }

                        clip.RemovePawnKeyframe(keyframe.keyframeID);
                    }
                }
            }

            Workspace.RecordEvent("Stage length");

            UpdateGUI();
        }

        public void UpdateGUI()
        {
            stageTimelineSlider.maxValue = Workspace.StageWindowSize;
            stageTimelineSlider.SetValueWithoutNotify(Workspace.StageTick);
            currentTimeField.SetTextWithoutNotify(Workspace.StageTick.ToString());
            stageWindowLengthField.SetTextWithoutNotify(Workspace.StageWindowSize.ToString());
            playBackSpeedField.SetTextWithoutNotify(Workspace.PlayBackSpeed.ToString());
        }
    }
}
