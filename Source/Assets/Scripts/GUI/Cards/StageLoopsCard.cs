﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class StageLoopsCard : MonoBehaviour
    {
        public InputField stageLoopsNormalField;
        public InputField stageLoopsQuickField;

        public void Start()
        {
            EventsManager.onAnimationTimelinesChanged.AddListener(delegate { UpdateGUI(); });
            EventsManager.onStageWindowSizeChanged.AddListener(delegate { UpdateGUI(); });

            stageLoopsNormalField.onEndEdit.AddListener(delegate { OnStageLoopsNormalFieldChange(); });
            stageLoopsQuickField.onEndEdit.AddListener(delegate { OnStageLoopsFastFieldChange(); });

            UpdateGUI();
        }

        public void OnStageLoopsNormalFieldChange()
        {
            if (Workspace.animationDef == null) return;

            Workspace.GetCurrentAnimationStage().StageLoopsNormal = int.Parse(stageLoopsNormalField.text);

            EventsManager.OnAnimationStageChanged(Workspace.GetCurrentAnimationStage());
            Workspace.RecordEvent("Cycle count (normal)");
        }

        public void OnStageLoopsFastFieldChange()
        {
            if (Workspace.animationDef == null) return;

            Workspace.GetCurrentAnimationStage().StageLoopsQuick = int.Parse(stageLoopsQuickField.text);

            EventsManager.OnAnimationStageChanged(Workspace.GetCurrentAnimationStage());
            Workspace.RecordEvent("Cycle count (fast)");
        }

        public void UpdateGUI()
        {
            stageLoopsNormalField.SetTextWithoutNotify(Workspace.GetCurrentAnimationStage().StageLoopsNormal.ToString());
            stageLoopsQuickField.SetTextWithoutNotify(Workspace.GetCurrentAnimationStage().StageLoopsQuick.ToString());
        }
    }
}
