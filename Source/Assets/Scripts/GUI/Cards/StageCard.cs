﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class StageCard : MonoBehaviour, IPointerClickHandler
    {
        public Text stageName;
        public InputField stageNameField;
        public Image banner;

        private int stageID { get { return transform.GetSiblingIndex(); } }

        public void Start()
        {
            EventsManager.onStageIDChanged.AddListener(delegate { Initialize(stageName.text); });
            stageNameField.onEndEdit.AddListener(delegate { OnNameChange(); });
        }

        public void Initialize(string stageName)
        {
            this.stageName.text = stageName;

            if (Workspace.StageID == transform.GetSiblingIndex())
            {
                banner.gameObject.SetActive(true);
            }

            else
            {
                banner.gameObject.SetActive(false);
                stageNameField.gameObject.SetActive(false);
            }
        }

        public void OnNameChange()
        {
            stageName.text = stageNameField.text;
            stageNameField.gameObject.SetActive(false);

            Workspace.GetCurrentAnimationStage().StageName = stageName.text;
            Workspace.RecordEvent("Stage renamed");
        }

        public void OnMoveStage(int delta)
        {        
            Workspace.animationDef.MoveAnimationStage(stageID, delta);        
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickCount >= 2)
            {
                stageNameField.text = stageName.text;
                stageNameField.gameObject.SetActive(true);
            }

            if (Workspace.StageID != transform.GetSiblingIndex())
            { Workspace.RecordEvent("Stage selected"); }

            Workspace.StageID = transform.GetSiblingIndex();
        }
    }
}