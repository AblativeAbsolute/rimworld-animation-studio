﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class ActorAddonCard : MonoBehaviour
    {
        public string addonName;
        public Text label;
        public Toggle toggle;
        public Dropdown anchorDropdown;
        public InputField anchoringPawnField;
        public Dropdown layerDropdown;
        public ActorAddonKeyframeCard actorAddonKeyframeCard;

        private ActorAddonDef actorAddonDef;

        private PawnAnimationClip clip { get { return Workspace.GetCurrentPawnAnimationClip(); } }

        private void Start()
        {
            
        }

        public void Initialize(ActorAddonDef actorAddonDef, ActorAddonKeyframeCard actorAddonKeyframeCard)
        {
            this.actorAddonDef = actorAddonDef;
            this.actorAddonKeyframeCard = actorAddonKeyframeCard;

            addonName = actorAddonDef.addonName;
            label.text = actorAddonDef.label;

            EventsManager.onAnimationChanged.AddListener(delegate { UpdateGUI(); });
            EventsManager.onActorIDChanged.AddListener(delegate { UpdateGUI(); });

            UpdateGUI();
        }

        public void UpdateGUI()
        {
            if (Workspace.animationDef == null || string.IsNullOrEmpty(addonName)) return;

            if (clip?.GetActorAddon(addonName) != null)
            {
                int i = Constants.bodyPartAnchorNames.Keys.ToList().IndexOf(clip.GetActorAddon(addonName).AnchorName);
                anchorDropdown.SetValueWithoutNotify(i);
                layerDropdown.SetValueWithoutNotify(layerDropdown.options.IndexOf(layerDropdown.options.First(x => x.text == clip.GetActorAddon(addonName).Layer)));
                anchoringPawnField.SetTextWithoutNotify(clip.GetActorAddon(addonName).AnchoringActor.ToString());
                toggle.SetIsOnWithoutNotify(clip.IsActorAddonVisible(addonName));

                anchoringPawnField.interactable = anchorDropdown.value != 0;
           }         
        }

        public void OnToggleChanged()
        {
            if (clip?.GetActorAddon(addonName) != null)
            { clip.GetActorAddon(addonName).render = toggle.isOn; }

            EventsManager.OnPawnKeyframeChanged(null);

            UpdateGUI();
        }

        public void OnAnchorChanged()
        {
            if (clip?.GetActorAddon(addonName) != null)
            { clip.GetActorAddon(addonName).AnchorName = Constants.bodyPartAnchorNames.Keys.ElementAt(anchorDropdown.value); }

            UpdateGUI();
        }

        public void OnLayerChanged()
        {
            if (clip?.GetActorAddon(addonName) != null)
            { clip.GetActorAddon(addonName).Layer = layerDropdown.options[layerDropdown.value].text; }
            
            UpdateGUI();
        }

        public void OnAnchoringPawnChanged()
        {
            if (clip?.GetActorAddon(addonName) != null)
            {
                int i = int.Parse(anchoringPawnField.text);

                if (i < 0) { i = clip.GetOwningActorID(); }
                i = Mathf.Clamp(i, 0, Workspace.animationDef.Actors.Count - 1);

                clip.GetActorAddon(addonName).AnchoringActor = i;
                anchoringPawnField.SetTextWithoutNotify(i.ToString());
            }          

            UpdateGUI();
        }
    }
}
