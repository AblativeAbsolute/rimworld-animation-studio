﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RimWorldAnimationStudio
{
    public class VersionText : MonoBehaviour
    {
        private void Start()
        {
            Text text = GetComponent<Text>();
            text.text = Constants.currentVersion;
        }
    }
}
