﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace RimWorldAnimationStudio
{
    public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public string message = "Undefined";
        public string executedCommand;
        public float delay = 0f;
        public Vector2 offset = new Vector2(5f, -15f);
        public bool flipX = false;

        public GameObject tooltip;
        public Text tooltipText;
        private bool isActive;
        private bool isDisplayed;
        private float activeTime = -1f;

        public void Start()
        {
            tooltip = Resources.FindObjectsOfTypeAll<TooltipMessage>()?.FirstOrDefault().gameObject;
            tooltipText = tooltip?.GetComponentInChildren<Text>();
        }

        public void Update()
        {
            if (tooltip == null || isActive == false || (activeTime + delay) > Time.unscaledTime) return;

            if (isDisplayed == false)
            {
                tooltip.GetComponent<RectTransform>().pivot = flipX ? new Vector2(1, 1) : new Vector2(0, 1);
                tooltipText.text = message;

                if (executedCommand != null && executedCommand != "")
                { tooltipText.text += " (" + KeybindConfig.GetKeybindLabel(executedCommand) + ")"; }

                tooltip.transform.position = (Vector2)transform.position + offset;
                tooltip.gameObject.SetActive(true);

                LayoutRebuilder.ForceRebuildLayoutImmediate(tooltip.GetComponent<RectTransform>());

                isDisplayed = true;
            }
        }

        public void OnDisable()
        {
            if (isActive)
            { OnPointerExit(new PointerEventData(EventSystem.current)); }
        }

        public void OnPointerEnter(PointerEventData pointerEventData)
        {
            isActive = true;
            activeTime = Time.unscaledTime;
        }

        public void OnPointerExit(PointerEventData pointerEventData)
        {
            isActive = false;
            isDisplayed = false;
            tooltip.gameObject.SetActive(false);
        }
    }
}
