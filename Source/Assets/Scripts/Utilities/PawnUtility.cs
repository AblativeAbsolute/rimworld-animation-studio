﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public static class PawnUtility
    {
		public static Vector3 BaseHeadOffsetAt(string bodyType, int rotation)
		{
			Vector2 headOffset = Vector3.zero;

			switch (bodyType)
			{
				case "Male":	headOffset = new Vector2(0.04f, 0.34f); break;
				case "Female":	headOffset = new Vector2(0.10f, 0.34f); break;
				case "Thin":	headOffset = new Vector2(0.09f, 0.34f); break;
				case "Hulk":	headOffset = new Vector2(0.10f, 0.34f); break;
				case "Fat":		headOffset = new Vector2(0.09f, 0.34f); break;
			}

			switch (rotation)
			{
				case 0: return new Vector3(0f, headOffset.y, 0);
				case 1: return new Vector3(headOffset.x, headOffset.y, 0);
				case 2: return new Vector3(0f, headOffset.y, 0f);
				case 3: return new Vector3(-headOffset.x, headOffset.y, 0f);
				default: return Vector3.zero;
			}
		}

		public static Vector3 GroinOffsetAt(string bodyType, int rotation)
		{
			if (rotation == 0 || rotation == 2)
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(0.00f, 0f, -0.46f);
					case "Female": return new Vector3(0.00f, 0f, -0.51f);
					case "Thin": return new Vector3(0.00f, 0f, -0.46f);
					case "Hulk": return new Vector3(0.00f, 0f, -0.66f);
					case "Fat": return new Vector3(0.00f, 0f, -0.52f);
					default: return Vector3.zero;
				}
			}

			else if (rotation == 1)
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(0.07f, 0f, -0.41f);
					case "Female": return new Vector3(0.07f, 0f, -0.47f);
					case "Thin": return new Vector3(-0.05f, 0f, -0.43f);
					case "Hulk": return new Vector3(-0.03f, 0f, -0.64f);
					case "Fat": return new Vector3(0.24f, 0f, -0.50f);
					default: return Vector3.zero;
				}
			}

			else
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(-0.07f, 0f, -0.41f);
					case "Female": return new Vector3(-0.07f, 0f, -0.47f);
					case "Thin": return new Vector3(0.05f, 0f, -0.43f);
					case "Hulk": return new Vector3(0.03f, 0f, -0.64f);
					case "Fat": return new Vector3(-0.24f, 0f, -0.50f);
					default: return Vector3.zero;
				}
			}
		}

		public static Vector3 BreastLeftOffsetAt(string bodyType, int rotation)
		{
			if (rotation == 0 || rotation == 2)
			{
				float multi = rotation == 0 ? -1f : 1f;

				switch (bodyType)
				{
					case "Male": return new Vector3(0.035f * multi, 0f, -0.050f);
					case "Female": return new Vector3(0.002f * multi, 0f, -0.044f);
					case "Thin": return new Vector3(0.000f * multi, 0f, -0.024f);
					case "Hulk": return new Vector3(0.118f * multi, 0f, -0.194f);
					case "Fat": return new Vector3(0.141f * multi, 0f, -0.084f);
					default: return Vector3.zero;
				}
			}

			else if (rotation == 1)
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(0.098f, 0f, -0.053f);
					case "Female": return new Vector3(0.008f, 0f, -0.008f);
					case "Thin": return new Vector3(-0.011f, 0f, -0.024f);
					case "Hulk": return new Vector3(0.180f, 0f, -0.151f);
					case "Fat": return new Vector3(0.200f, 0f, -0.034f);
					default: return Vector3.zero;
				}
			}

			else
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(-0.098f, 0f, -0.053f);
					case "Female": return new Vector3(-0.008f, 0f, -0.008f);
					case "Thin": return new Vector3(0.011f, 0f, -0.024f);
					case "Hulk": return new Vector3(-0.180f, 0f, -0.151f);
					case "Fat": return new Vector3(-0.200f, 0f, -0.034f);
					default: return Vector3.zero;
				}
			}
		}

		public static Vector3 BreastRightOffsetAt(string bodyType, int rotation)
		{
			if (rotation == 0 || rotation == 2)
			{
				float multi = rotation == 0 ? -1f : 1f;

				switch (bodyType)
				{
					case "Male": return new Vector3(-0.035f * multi, 0f, -0.050f);
					case "Female": return new Vector3(-0.002f * multi, 0f, -0.044f);
					case "Thin": return new Vector3(-0.000f * multi, 0f, -0.024f);
					case "Hulk": return new Vector3(-0.118f * multi, 0f, -0.194f);
					case "Fat": return new Vector3(-0.141f * multi, 0f, -0.084f);
					default: return Vector3.zero;
				}
			}

			else if (rotation == 1)
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(0.098f, 0f, -0.053f);
					case "Female": return new Vector3(0.008f, 0f, -0.008f);
					case "Thin": return new Vector3(-0.011f, 0f, -0.024f);
					case "Hulk": return new Vector3(0.180f, 0f, -0.151f);
					case "Fat": return new Vector3(0.200f, 0f, -0.034f);
					default: return Vector3.zero;
				}
			}

			else
			{
				switch (bodyType)
				{
					case "Male": return new Vector3(-0.098f, 0f, -0.053f);
					case "Female": return new Vector3(-0.008f, 0f, -0.008f);
					case "Thin": return new Vector3(0.011f, 0f, -0.024f);
					case "Hulk": return new Vector3(-0.180f, 0f, -0.151f);
					case "Fat": return new Vector3(-0.200f, 0f, -0.034f);
					default: return Vector3.zero;
				}
			}
		}

		public static Vector3 GetBodyPartAnchor(ActorBody anchoringActorBody, string anchorName)
		{
			Actor anchoringActor = Workspace.GetActor(anchoringActorBody.actorID);
			Vector3 anchoringActorBodyPos = anchoringActorBody.transform.position;
			Quaternion anchoringActorBodyQuad = Quaternion.AngleAxis(anchoringActorBody.transform.rotation.eulerAngles.z, Vector3.forward);
			int anchoringActorFacing = anchoringActor.GetCurrentPosition().bodyFacing;

			switch (anchorName)
			{
				case "torso": return anchoringActorBodyPos;
				case "head": return anchoringActorBody.GetComponentsInChildren<ActorBodyPart>().FirstOrDefault(x => x.bodyPart.ToLower() == "head").transform.position;
				case "groin": return anchoringActorBodyPos + anchoringActorBodyQuad * PawnUtility.GroinOffsetAt(anchoringActor.bodyType, anchoringActorFacing).FlipAxes();
				case "left breast": return anchoringActorBodyPos + anchoringActorBodyQuad * PawnUtility.BreastLeftOffsetAt(anchoringActor.bodyType, anchoringActorFacing).FlipAxes();
				case "right breast": return anchoringActorBodyPos + anchoringActorBodyQuad * PawnUtility.BreastRightOffsetAt(anchoringActor.bodyType, anchoringActorFacing).FlipAxes();
				default: return new Vector3();
			}
		}
	}
}
