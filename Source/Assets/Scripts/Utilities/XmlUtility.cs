﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public static class XmlUtility
    {
        public static T ReadXML<T>(string path)
        {
            using (StreamReader stringReader = new StreamReader(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                var data = (T)serializer.Deserialize(stringReader);

                return data;
            }
        }

        public static void WriteXML<T>(T obj, string path)
        {
            if (obj == null || path == null || path == "")
            { return; }

            XmlSerializer writer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();
            nameSpaces.Add("", "");

            FileStream file = File.Create(path);
            writer.Serialize(file, obj, nameSpaces);
            file.Close();
        }
    }
}
