﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace RimWorldAnimationStudio
{
    public class AudioController : MonoBehaviour
    {
        private AudioSource audioSource;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();        
        }

        public void PlaySound(string soundDefName)
        {
            if (string.IsNullOrEmpty(soundDefName) || soundDefName.ToLower() == "none")
            { return; }

            SoundDef soundDef = SoundDefs.GetNamed(soundDefName);

            if (soundDef == null)
            {
                Debug.LogWarning("Could not play audio clip - SoundDef '" + soundDefName + "' was not found");
                return;
            }

            foreach (SubSoundDef subSoundDef in soundDef.subSounds)
            {
                AudioGrain audioGrain = subSoundDef.grains[UnityEngine.Random.Range(0, subSoundDef.grains.Count - 1)];

                string fullPath = Path.GetFullPath(Path.Combine(Application.streamingAssetsPath, "Sounds", audioGrain.clipPath)) + ".wav";
                AudioClip audioClip = SoundDefs.GetAudioClip(fullPath);

                if (audioClip == null) return;
                
                // Set up audio source to play
                audioSource.clip = audioClip;
                audioSource.volume = UnityEngine.Random.Range(subSoundDef.volumeRange.min, subSoundDef.volumeRange.max);
                audioSource.minDistance = subSoundDef.distRange.min;
                audioSource.maxDistance = subSoundDef.distRange.max;
                audioSource.pitch = UnityEngine.Random.Range(subSoundDef.pitchRange.min, subSoundDef.pitchRange.max);

                audioSource.Play();
            }
        }
    }
}
