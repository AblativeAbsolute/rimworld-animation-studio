﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TransformExtensions
{
    public static Transform FindDeepChild(this Transform parent, string childName)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(parent);

        while (queue.Count > 0)
        {
            var c = queue.Dequeue();
            if (c.name == childName)
                return c;
            foreach (Transform t in c)
                queue.Enqueue(t);
        }

        return null;
    }
}

