﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public static class Vector3Extension
    {
        public static Vector3 FlipAxes(this Vector3 v)
        {
            return new Vector3(v.x, v.z, v.y);
        }
    }
}
