﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public class KeybindConfig
    {
        private static List<Keybind> keybinds = new List<Keybind>();
        private static bool initialized = false;

        public static void Initialize()
        {
            string path = Path.Combine(Application.streamingAssetsPath, "keybindConfig.xml");
            keybinds = XmlUtility.ReadXML<List<Keybind>>(path);
            initialized = true;
        }

        public static List<Keybind> GetAllKeybinds()
        {
            if (initialized == false)
            { Initialize(); }

            return keybinds;
        }

        public static string GetKeybindLabel(string command)
        {
            string label = "";
            Keybind keybind = GetAllKeybinds()?.FirstOrDefault(x => x.command == command);

            if (keybind == null) return label;

            List<KeyCode> keyModifiers = keybind.keyModifiers;
            KeyCode keyCode = keybind.keyCode;

            foreach (KeyCode modKeyCode in keyModifiers)
            {
                switch (modKeyCode)
                {
                    case KeyCode.LeftShift: label += "Shift + "; break;
                    case KeyCode.LeftControl: label += "Ctrl + "; break;
                    case KeyCode.LeftAlt: label += "Alt + "; break;
                    case KeyCode.LeftCommand: label += "Cmd + "; break;
                    case KeyCode.RightShift: label += "Right shift + "; break;
                    case KeyCode.RightControl: label += "Right ctrl + "; break;
                    case KeyCode.RightAlt: label += "Right alt + "; break;
                    case KeyCode.RightCommand: label += "Right cmd + "; break;
                }
            }

            return label += keyCode.ToString();
        }
    }
}
