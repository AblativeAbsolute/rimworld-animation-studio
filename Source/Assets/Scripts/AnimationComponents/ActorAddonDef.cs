﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RimWorldAnimationStudio
{
    public class ActorAddonDef
    {
        public string addonName;
        public string label;
        public float scale = 1f;

        public GraphicData graphicData;
    }
}
