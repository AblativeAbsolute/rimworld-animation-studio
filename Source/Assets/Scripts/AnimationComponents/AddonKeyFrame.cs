﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public class AddonKeyframe
    {
        // Data to/from animationDef
        public string addonName;
        public float? posX;
        public float? posZ;
        public float? rotation;

        // Data serialization control
        public bool ShouldSerializeposX() { return posX.HasValue; }
        public bool ShouldSerializeposZ() { return posZ.HasValue; }
        public bool ShouldSerializerotation() { return rotation.HasValue; }


        // Data helper functions
        [XmlIgnore] public string AddonName 
        {  
            get { return addonName; } 
            set { addonName = value; } 
        }

        [XmlIgnore] public float PosX
        {
            get { return posX.HasValue ? posX.Value : 0f; }
            set { posX = value; }
        }

        [XmlIgnore] public float PosZ
        {
            get { return posZ.HasValue ? posZ.Value : 0f; }
            set { posZ = value; }
        }

        [XmlIgnore] public float Rotation
        {
            get { return rotation.HasValue ? rotation.Value : 0f; }
            set { rotation = value; }
        }

        // Constructors
        public AddonKeyframe() { }

        public AddonKeyframe(string addonName)
        {
            this.AddonName = addonName;
        }
    }
}
