﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public class ActorAddon
    {
        // Data to/from animationDef
        public string addonName;
        public int? anchoringActor;
        public string anchorName;
        public string layer = "Pawn";
        public GraphicData graphicData;
        public bool? render;

        // Data serialization control
        public bool ShouldSerializeanchorName() { return string.IsNullOrEmpty(anchorName) == false && anchorName.ToLower() != "none"; }
        public bool ShouldSerializeanchoringActor() { return anchoringActor.HasValue; }
        public bool ShouldSerializerender() { return render == true; }

        // Data helper functions
        [XmlIgnore] public string AddonName
        {
            get { return addonName; }
            set { addonName = value; }
        }

        [XmlIgnore] public int AnchoringActor
        {
            get { return anchoringActor.HasValue ? anchoringActor.Value : 0; }
            set { anchoringActor = value; }
        }

        [XmlIgnore] public string AnchorName
        {
            get { return anchorName; }
            set { anchorName = value; }
        }

        [XmlIgnore] public string Layer
        {
            get { return layer; }
            set { layer = value; }
        }

        [XmlIgnore]
        public GraphicData GraphicData
        {
            get { return graphicData; }
            set { graphicData = value; }
        }

        [XmlIgnore] public bool Render
        {
            get { return render == true; }
            set { render = value; }
        }

        // Simple curves
        [XmlIgnore] public SimpleCurve PosX = new SimpleCurve();
        [XmlIgnore] public SimpleCurve PosZ = new SimpleCurve();
        [XmlIgnore] public SimpleCurve Rotation = new SimpleCurve();

        // Constructors
        public ActorAddon() { }

        public ActorAddon(ActorAddonDef actorAddonDef)
        {
            this.AddonName = actorAddonDef.addonName;
            this.GraphicData = actorAddonDef.graphicData.Copy();
        }
    }
}
