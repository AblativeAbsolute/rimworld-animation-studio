﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    [Serializable]
    public class PawnRaceDef
    {
        // Local data
        public string defName;
        public bool isHumanoid = true;
        public float scale = 1f;

        public List<MultiDirectionalGraphic> bodyTypeGraphics = new List<MultiDirectionalGraphic>();
        public MultiDirectionalGraphic headGraphics = new MultiDirectionalGraphic();

        // Constructors
        public PawnRaceDef() { }

        public PawnRaceDef(string defName)
        {
            this.defName = defName;
        }

        // Methods
        public Sprite GetHeadGraphic(CardinalDirection facing)
        {
            if (HasValidHeadGraphicPath(facing) == false)
            { return null; }

            switch (facing)
            {
                case CardinalDirection.North: return headGraphics.northGraphic.sprite;
                case CardinalDirection.East: return headGraphics.eastGraphic.sprite;
                case CardinalDirection.South: return headGraphics.southGraphic.sprite;
                default: return headGraphics.eastGraphic.sprite;
            }
        }

        public Sprite GetBodyTypeGraphic(CardinalDirection facing, string bodyType = "None")
        {
            if (HasValidBodyTypeGraphicPath(facing, bodyType) == false)
            { return null; }

            MultiDirectionalGraphic bodyTypeGraphic = bodyTypeGraphics.FirstOrDefault(x => x.bodyType == bodyType);
            
            if (bodyTypeGraphic == null)
            {
                bodyTypeGraphic = new MultiDirectionalGraphic(bodyType);
                bodyTypeGraphics.Add(bodyTypeGraphic);
            }

            switch (facing)
            {
                case CardinalDirection.North: return bodyTypeGraphic.northGraphic.sprite;
                case CardinalDirection.East: return bodyTypeGraphic.eastGraphic.sprite;
                case CardinalDirection.South: return bodyTypeGraphic.southGraphic.sprite;
                default: return bodyTypeGraphic.eastGraphic.sprite;
            }
        }

        public string GetHeadGraphicPath(CardinalDirection facing)
        {
            if (HasValidHeadGraphicPath(facing) == false)
            { return "Invalid file path"; }

            switch (facing)
            {
                case CardinalDirection.North: return headGraphics.northGraphic.path;
                case CardinalDirection.East: return headGraphics.eastGraphic.path;
                case CardinalDirection.South: return headGraphics.southGraphic.path;
                default: return headGraphics.eastGraphic.path;
            }
        }

        public string GetBodyTypeGraphicPath(CardinalDirection facing, string bodyType = "None")
        {
            if (HasValidBodyTypeGraphicPath(facing, bodyType) == false)
            { return "Invalid file path"; }

            MultiDirectionalGraphic bodyTypeGraphic = bodyTypeGraphics.FirstOrDefault(x => x.bodyType == bodyType);

            if (bodyTypeGraphic == null)
            {
                bodyTypeGraphic = new MultiDirectionalGraphic(bodyType);
                bodyTypeGraphics.Add(bodyTypeGraphic);
            }

            switch (facing)
            {
                case CardinalDirection.North: return bodyTypeGraphic.northGraphic.path;
                case CardinalDirection.East: return bodyTypeGraphic.eastGraphic.path;
                case CardinalDirection.South: return bodyTypeGraphic.southGraphic.path;
                default: return bodyTypeGraphic.eastGraphic.path;
            }
        }

        public void SetHeadGraphicPath(string path, CardinalDirection facing)
        {
            switch (facing)
            {
                case CardinalDirection.North: headGraphics.northGraphic.SetPath(path); break;
                case CardinalDirection.East: headGraphics.eastGraphic.SetPath(path); break;
                case CardinalDirection.South: headGraphics.southGraphic.SetPath(path); break;
                default: headGraphics.eastGraphic.SetPath(path); break;
            }
        }

        public void SetBodyTypeGraphicPath(string path, CardinalDirection facing, string bodyType = "None")
        {
            MultiDirectionalGraphic bodyTypeGraphic = bodyTypeGraphics.FirstOrDefault(x => x.bodyType == bodyType);

            if (bodyTypeGraphic == null)
            {
                bodyTypeGraphic = new MultiDirectionalGraphic(bodyType);
                bodyTypeGraphics.Add(bodyTypeGraphic);
            }

            switch (facing)
            {
                case CardinalDirection.North: bodyTypeGraphic.northGraphic.SetPath(path); break;
                case CardinalDirection.East: bodyTypeGraphic.eastGraphic.SetPath(path); break;
                case CardinalDirection.South:bodyTypeGraphic.southGraphic.SetPath(path); break;
                default: bodyTypeGraphic.eastGraphic.SetPath(path); break;
            }
        }

        public bool HasValidHeadGraphicPath(CardinalDirection facing)
        {
            return headGraphics.HasValidPathForDirection(facing);
        }

        public bool HasValidBodyTypeGraphicPath(CardinalDirection facing, string bodyType = "None")
        {
            MultiDirectionalGraphic bodyTypeGraphic = bodyTypeGraphics.FirstOrDefault(x => x.bodyType == bodyType);

            if (bodyTypeGraphic == null)
            {
                bodyTypeGraphic = new MultiDirectionalGraphic(bodyType);
                bodyTypeGraphics.Add(bodyTypeGraphic);
            }

            return bodyTypeGraphic.HasValidPathForDirection(facing);
        }
    }
}
