﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static bool shuttingDown = false;
        private static object threadLock = new object();
        private static T instance;

        public static T Instance
        {
            get
            {
                if (shuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) + "' already destroyed. Returning null.");
                    return null;
                }

                lock (threadLock)
                {
                    if (instance == null)
                    {
                        instance = (T)FindObjectOfType(typeof(T));

                        if (instance == null)
                        {
                            var singletonObject = new GameObject();
                            instance = singletonObject.AddComponent<T>();
                            singletonObject.name = typeof(T).ToString() + " (Singleton)";
                        }
                    }

                    return instance;
                }
            }
        }

        private void OnDestroy()
        {
            shuttingDown = true;
        }
    }
}