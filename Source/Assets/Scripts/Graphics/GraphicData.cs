﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using UnityEngine;

namespace RimWorldAnimationStudio
{
    [Serializable]
    public class GraphicData
    {
        public string texPath;
        public string graphicClass;
        public string shaderType;
        public string drawSize;

        private Sprite sprite;

        public void SetDrawSize(Vector2 drawSize)
        {
            this.drawSize = "(" + drawSize.x + ", " + drawSize.y + ")";
        }

        public Vector3 GetDrawSize()
        {
            string drawSizeString = drawSize;

            drawSizeString = drawSizeString.Trim();
            drawSizeString = drawSizeString.Replace("(", "");
            drawSizeString = drawSizeString.Replace(")", "");
            var drawSizeStrings = drawSizeString.Split(',');

            return new Vector3(float.Parse(drawSizeStrings[0]), float.Parse(drawSizeStrings[1]));
        }

        public Sprite GetSprite()
        {
            if (sprite != null) return sprite;

            if (string.IsNullOrEmpty(texPath)) return null;

            string fullPath = Path.GetFullPath(Path.Combine(Application.streamingAssetsPath, texPath)) + ".png";

            if (File.Exists(fullPath) == false) return null;

            byte[] pngBytes = File.ReadAllBytes(fullPath);

            Texture2D texture = new Texture2D(2, 2);
            texture.LoadImage(pngBytes);

            float scale = Mathf.Min(texture.width, texture.height) / 128f;
            sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 85.0f * scale);

            return sprite;
        }
    }
}
