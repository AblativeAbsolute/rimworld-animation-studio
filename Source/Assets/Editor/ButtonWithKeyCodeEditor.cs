﻿using UnityEngine;
using UnityEditor;

namespace RimWorldAnimationStudio
{
    [CustomEditor(typeof(ButtonWithKeyCode))]
    [CanEditMultipleObjects]
    public class ButtonWithKeyCodeEditor : UnityEditor.UI.ButtonEditor
    {
        SerializedProperty keyCodeProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            keyCodeProp = serializedObject.FindProperty("keyCode");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(keyCodeProp, new GUIContent("KeyCode"));

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}
