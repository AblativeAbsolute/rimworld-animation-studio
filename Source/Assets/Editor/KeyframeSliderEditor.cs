﻿using UnityEngine;
using UnityEditor;

namespace RimWorldAnimationStudio
{
    [CustomEditor(typeof(KeyframeSlider))]
    [CanEditMultipleObjects]
    public class KeyframeSliderEditor : UnityEditor.UI.SliderEditor
    {
        SerializedProperty ghostSlidersProp;
        SerializedProperty ghostSliderPrefabProp;
        SerializedProperty handleImageProp;
        SerializedProperty soundIconProp;
        SerializedProperty maxGhostsProp;

        protected override void OnEnable()
        {
            base.OnEnable();
            ghostSlidersProp = serializedObject.FindProperty("ghostSliders");
            ghostSliderPrefabProp = serializedObject.FindProperty("ghostSliderPrefab");
            handleImageProp = serializedObject.FindProperty("handleImage");
            soundIconProp = serializedObject.FindProperty("soundIcon");
            maxGhostsProp = serializedObject.FindProperty("maxGhosts");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.PropertyField(ghostSlidersProp, new GUIContent("Ghost Sliders"));
            EditorGUILayout.PropertyField(ghostSliderPrefabProp, new GUIContent("Ghost Slider Prefab"));
            EditorGUILayout.PropertyField(handleImageProp, new GUIContent("Handle Image"));
            EditorGUILayout.PropertyField(soundIconProp, new GUIContent("Sound Icon"));
            EditorGUILayout.PropertyField(maxGhostsProp, new GUIContent("Max Ghosts"));

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}
